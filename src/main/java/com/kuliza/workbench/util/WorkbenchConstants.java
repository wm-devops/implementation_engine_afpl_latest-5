package com.kuliza.workbench.util;

public class WorkbenchConstants {
  // **************** Workbench URLs *************************

  public static final String WORKBENCH_API_PREFIX = "/workbench/api";
  public static final String WORKBENCH_STARTER_API_PREFIX = "/getting-started";
  public static final String WORKBENCH_STARTER_PROP_ENDPOINT = "/global-props";
}
